// ignore_for_file: file_names

import 'dart:io';

void main(List<String> args) {
  print("Menu");
  print("Select the choice you want to perform :");
  print("1. ADD");
  print("2. SUBTRACT");
  print("3. MULTIPLY");
  print("4. DIVIDE");
  print("5. EXIT");
  print("Choice you want to enter :");
  int? choice = int.parse(stdin.readLineSync()!);
  switch (choice) {
    case 1:
      {
        print("Enter the value for x :");
        int? x = int.parse(stdin.readLineSync()!);
        print("Enter the value for y :");
        int? y = int.parse(stdin.readLineSync()!);
        print("Sum of the two number is :");
        print(x + y);
        break;
      }
    case 2:
      {
        print("Enter the value for x :");
        int? x = int.parse(stdin.readLineSync()!);
        print("Enter the value for y :");
        int? y = int.parse(stdin.readLineSync()!);
        print("Subtraction result of the two number is :");
        print(x - y);
        break;
      }
    case 3:
      {
        print("Enter the value for x :");
        int? x = int.parse(stdin.readLineSync()!);
        print("Enter the value for y :");
        int? y = int.parse(stdin.readLineSync()!);
        print("Multiplication result of the two number is :");
        print(x * y);
        break;
      }
    case 4:
      {
        print("Enter the value for x :");
        int? x = int.parse(stdin.readLineSync()!);
        print("Enter the value for y :");
        int? y = int.parse(stdin.readLineSync()!);
        print("Division result of the two number is :");
        print(x / y);
        break;
      }
    case 5:
      {
        break;
      }
  }
}
