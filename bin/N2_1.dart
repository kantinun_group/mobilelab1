// ignore_for_file: file_names

import 'dart:io';

void main(List<String> args) {
  String? input = stdin.readLineSync()!;
  List<String> wordList = input.toLowerCase().split(' ');
  var seen = Set<String>();
  List<String> uniqueWordList =
      wordList.where((word) => seen.add(word)).toList();
  for (var word in uniqueWordList) {
    print(word);
  }
}
